const {factorial, div_check} = require('../src/util.js');
const { expect, assert } = require('chai');

describe('test_fun_factorials',() => {
	it('Test that is 0! is 1', () => {
		const product = factorial(0);
		assert.equal(product, 1);
	})

	it('Test that is 4! is 24', () => {
		const product = factorial(4);
		expect(product).to.equal(24);
	})

	it('Test that is 10! is 3628800', () => {
		const product = factorial(10);
		expect(product).to.equal(3628800);
	})	
});

describe('Check 5 and 7 Divisibility', () => {
	it('Asssert that 105 is divisible by 5', () => {
		const divisible = div_check(105);
		expect(divisible).to.equal(true)
	});

	it('Assert that 14 is divisible by 7', () => {
		const divisible = div_check(14);
		expect(divisible).to.equal(true);
	})

	it('Assert that 0 is divisible by both 5 and 7', () => {
		const divisible = div_check(0);
		expect(divisible).to.equal(true);
	})

	it('Assert that 22 is NOT divisible by both 5 and 7', () => {
		const divisible = div_check(22);
		expect(divisible).to.equal(false);
	})
})